//
//  Confimation.swift
//  PureeSample
//
//  Created by 山本航 on 2021/08/20.
//

import Foundation
import Puree

struct ConfigurationManager {

    static let configuration = Logger.Configuration(
        filterSettings: [
            FilterSetting {
                LogFilter(tagPattern: TagPattern(string: "hogehoge")!, options: nil)
            }
        ],
        outputSettings: [
            OutputSetting {
                LogServerOutput(logStore: $0, tagPattern: TagPattern(string: "hogehoge")!, options: nil)
            },
            OutputSetting {
                ConsoleOutput(logStore: $0, tagPattern: TagPattern(string: "fugafuga")!, options: nil)
            },
        ])
}
