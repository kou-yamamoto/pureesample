//
//  ConsoleOutput.swift
//  PureeSample
//
//  Created by 山本航 on 2021/08/19.
//

import Foundation
import Puree

final class ConsoleOutput: InstantiatableOutput {

    var tagPattern: TagPattern
    var logStore: LogStore

    init(logStore: LogStore, tagPattern: TagPattern, options: OutputOptions?) {
        self.logStore = logStore
        self.tagPattern = tagPattern
    }

    func emit(log: LogEntry) {
        if let userData = log.userData {
            print(log.date)
            let jsonObject = try! JSONSerialization.jsonObject(with: userData)
            print(jsonObject)
        }
    }
}

extension ConsoleOutput {

    // プラグインが最初に設定された時に呼ばれる
    func start() {
    }

    // アプリがバックグラウンドに入った時に呼ばれる
    func suspend() {
    }

    // アプリがフォアグラウンドに戻った時に呼ばれる
    func resume() {
    }
}
