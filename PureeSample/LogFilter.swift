//
//  LogFilter.swift
//  PureeSample
//
//  Created by 山本航 on 2021/08/19.
//

import Foundation
import Puree

struct LogFilter: InstantiatableFilter {

    let tagPattern: TagPattern
    
    init(tagPattern: TagPattern, options: FilterOptions?) {
        self.tagPattern = tagPattern
    }

    func convertToLogs(_ payload: [String : Any]?, tag: String, captured: String?, logger: Logger) -> Set<LogEntry> {
        let currentDate = logger.currentDate

        let userData: Data?

        if let payload = payload {
            userData = try! JSONSerialization.data(withJSONObject: payload)
        } else {
            userData = nil
        }

        let log = LogEntry(tag: tag, date: currentDate, userData: userData)
        return [log]
    }
}
