//
//  BufferedOutput.swift
//  PureeSample
//
//  Created by 山本航 on 2021/08/19.
//

import Foundation
import Puree

final class LogServerOutput: BufferedOutput {

    let logStore: LogStore

    required init(logStore: LogStore, tagPattern: TagPattern, options: OutputOptions? = nil) {

        self.logStore = logStore
        super.init(logStore: logStore, tagPattern: tagPattern)

        // ここでバッファの設定をする
        // ログのデータサイズ
        self.configuration.chunkDataSizeLimit = nil
        // バッファリングする数
        self.configuration.logEntryCountLimit = 5
        // リトライ回数
        self.configuration.retryLimit = 0
        // ログ出力の回数
        self.configuration.flushInterval = 100
    }

    override func write(_ chunk: BufferedOutput.Chunk, completion: @escaping (Bool) -> Void) {

        let payload: [Any] = chunk.logs.compactMap { log in
            if let userData = log.userData {
                return try? JSONSerialization.jsonObject(with: userData, options: [])
            }
            return nil
        }

        if let data = try? JSONSerialization.data(withJSONObject: payload, options: []) {
            // API通信はここで行う

            // API通信に成功した場合 → completion(true)

            // API通信に失敗した場合 → completion(false)
            // completion(false)とした場合、自動的にログがキャッシュに溜まり、次回のwriteメソッド呼び出し時にリトライされる。
        }
    }
}
