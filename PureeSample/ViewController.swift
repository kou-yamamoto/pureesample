//
//  ViewController.swift
//  PureeSample
//
//  Created by 山本航 on 2021/08/19.
//

import UIKit
import Puree

final class ViewController: UIViewController {

    private var logger: Logger!

    override func viewDidLoad() {
        super.viewDidLoad()
        let logger = try! Logger(configuration: ConfigurationManager.configuration)
        self.logger = logger
    }

    @IBAction func sendLogButtonDidTapped(_ sender: Any) {
        self.logger.postLog(["test": "test"], tag: "hogehoge")
    }

    @IBAction func sendLog2ButtonDidTapped(_ sender: Any) {
        self.logger.postLog(["aaaa": "aaaa"], tag: "hogehoge")
    }
}
